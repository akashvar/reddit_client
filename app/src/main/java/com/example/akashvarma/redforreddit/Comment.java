package com.example.akashvarma.redforreddit;

import com.google.gson.JsonObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Comment {

    private String BASE_URL = "https://oauth.reddit.com/";

    private String id;

    private String content;

    private String author;

    private String votes;

    private String commentTime;

    private List<String> hiddenChildren;

    private int level;

    public Comment() { }

    public List<String> getHiddenChildren() {
        return hiddenChildren;
    }

    public void setHiddenChildren(List<String> hiddenChildren) {
        this.hiddenChildren = hiddenChildren;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getVotes() {
        return votes;
    }

    public void setVotes(String votes) {
        this.votes = votes;
    }

    public String getCommentTime() {
        return commentTime;
    }

    public void setCommentTime(String commentTime) {
        this.commentTime = commentTime;
    }

    public void upvote() {

        Retrofit.Builder b = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = b.build();

        final RedditClient client = retrofit.create(RedditClient.class);

        //String token = AuthHeaders.ACCESS_TOKEN;

        Call<JsonObject> upvoteCall = client.castVote(
                AuthHeaders.ACCESS_TOKEN,
                this.getId(),
                1);

        upvoteCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                //Toast.makeText(, "Oops something went wrong!", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void downvote() {

        Retrofit.Builder b = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = b.build();

        final RedditClient client = retrofit.create(RedditClient.class);

        //String token = AuthHeaders.ACCESS_TOKEN;

        Call<JsonObject> downvoteCall = client.castVote(
                AuthHeaders.ACCESS_TOKEN,
                this.getId(),
                -1);

        downvoteCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                //Toast.makeText(, "Oops something went wrong!", Toast.LENGTH_LONG).show();
            }
        });
    }
}
