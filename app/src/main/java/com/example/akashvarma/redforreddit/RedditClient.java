package com.example.akashvarma.redforreddit;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RedditClient {

    @Headers("Accept: application/json")
    @POST("api/v1/access_token")
    @FormUrlEncoded
    Call<AccessToken> getAccessToken(
            @Header("Authorization") String authorization,
            @Field("grant_type") String authCode,
            @Field("code") String code,
            @Field("redirect_uri") String redirectUri
    );

    @POST("api/v1/access_token")
    @FormUrlEncoded
    Call<AccessToken> refreshToken(
            @Header("Authorization") String authorization,
            @Field("grant_type") String authCode,
            @Field("refresh_token") String refreshToken
    );

    @GET(".")
    @Headers("Accept: application/json")
    Call<JsonObject> getFrontPage(
            @Header("Authorization") String token,
            @Query("show") String value
    );

    @POST("api/vote")
    Call<JsonObject> castVote(
            @Header("Authorization") String token,
            @Query("id") String Id,
            @Query("dir") int vote
    );

    @GET("{subreddit}/comments/{id}")
    @Headers("Accept: application/json")
    Call<JsonArray> getComments(
            @Header("Authorization") String token,
            @Path("subreddit") String subreddit,
            @Path("id") String id
    );

    @GET("/search")
    @Headers("Accept: application/json")
    Call<JsonObject> fetchSearchResults(
            @Header("Authorization") String token,
            @Query("q") String query
    );




}

