package com.example.akashvarma.redforreddit;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity {

    private SharedPreferences oauthPrefs;

    private Button loginButton;
    private Button homeButton;

    //reddit oauth strings
    String clientId = "HLcckWPhhKQpYg";
    String clientSecret = "oOd6LLGDeWvu2JnhHFLiWoA7bSA";
    String redirectUrl = "https://test.example.myapp";

    private String httpAuthHeader;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginButton = findViewById(R.id.login_button);
        homeButton = findViewById(R.id.back_to_home);

        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginActivity.this.authenticate();
            }
        });

        oauthPrefs = getSharedPreferences("MYOAUTHPREFERENCES", MODE_PRIVATE);
        // PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        byte[] data = (clientId + ":" + clientSecret).getBytes();
        httpAuthHeader = "Basic " + Base64.encodeToString(data, Base64.NO_WRAP);
    }

    private void authenticate() {

        Intent oauthIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.reddit.com/api/v1/authorize.compact" + "?client_id=" + clientId + "&response_type=code" +
                "&state=blah" + "&redirect_uri=" + redirectUrl + "&duration=permanent" + "&scope=mysubreddits read vote"));
        oauthIntent.setPackage("com.android.chrome");
        startActivity(oauthIntent);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Uri uri = this.getIntent().getData();

        if (uri != null && uri.toString().startsWith(redirectUrl)) {

            String code = uri.getQueryParameter("code");

            Retrofit.Builder builder = new Retrofit.Builder()
                    .baseUrl("https://www.reddit.com/")
                    .addConverterFactory(GsonConverterFactory.create());

            Retrofit retrofit = builder.build();

            final RedditClient client = retrofit.create(RedditClient.class);


            Call<AccessToken> accessTokenCall = client.getAccessToken(
                    httpAuthHeader,
                    "authorization_code",
                    code,
                    redirectUrl
            );

            accessTokenCall.enqueue(new Callback<AccessToken>() {
                @Override
                public void onResponse(Call<AccessToken> call, Response<AccessToken> response) {

                    if (response.code() == 200) {


                        String token = "Bearer " + response.body().getAccessToken();
                        AuthHeaders.ACCESS_TOKEN = token;
                        AuthHeaders.REFRESH_TOKEN = response.body().getRefreshToken();


                        SharedPreferences.Editor edit = oauthPrefs.edit();

                        edit.putString("AuthHeader", httpAuthHeader);
                        edit.putString("AccessToken", token);
                        edit.putString("RefreshToken", response.body().getRefreshToken());
                        edit.putLong("Expiration", Long.parseLong(response.body().getExpiryTime()) + (System.currentTimeMillis() / 1000));
                        edit.commit();

                        Toast.makeText(LoginActivity.this, "successful token retrieval", Toast.LENGTH_LONG).show();

                        //finish();

                    }

                }

                @Override
                public void onFailure(Call<AccessToken> call, Throwable t) {

                    Toast.makeText(LoginActivity.this, "Something went wrong!", Toast.LENGTH_LONG).show();
                }
            });
        }
        //finish();
    }
}
