package com.example.akashvarma.redforreddit;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;
import com.google.gson.annotations.Expose;
import com.unnamed.b.atv.model.TreeNode;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class Post implements Serializable {

    private String BASE_URL = "https://oauth.reddit.com/";

    public String title;

    public String content;

    public String link;

    public String author;

    public String thumbnail;

    public String lastUpdated;

    public String id;

    public String subreddit;

    public Boolean voted;

    public String domain;

    public Post() { }

    public Post(String id, String link, String title, String content, String subreddit, String author, String domain) {

        this.id = id;
        this.link = link;
        this.title = title;
        this.content = content;
        this.subreddit = subreddit;
        this.author = author;
        this.domain = domain;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public Boolean getVoted() {
        return voted;
    }

    public void setVoted(Boolean voted) {
        this.voted = voted;
    }

    public String getSubreddit() { return subreddit; }

    public void setSubreddit(String subreddit) { this.subreddit = subreddit; }

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }

    public String getThumbnail() { return thumbnail; }

    public void setThumbnail(String thumbnail) { this.thumbnail = thumbnail; }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }



    public void upvote() {

        Retrofit.Builder b = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = b.build();

        final RedditClient client = retrofit.create(RedditClient.class);

        String token = AuthHeaders.ACCESS_TOKEN;

        Call<JsonObject> upvoteCall = client.castVote(
                AuthHeaders.ACCESS_TOKEN,
                this.getId(),
                1);

        upvoteCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                //Toast.makeText(, "Oops something went wrong!", Toast.LENGTH_LONG).show();
            }
        });

    }

    public void downvote() {

        Retrofit.Builder b = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = b.build();

        final RedditClient client = retrofit.create(RedditClient.class);

        String token = AuthHeaders.ACCESS_TOKEN;

        Call<JsonObject> downvoteCall = client.castVote(
                AuthHeaders.ACCESS_TOKEN,
                this.getId(),
                -1);

        downvoteCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                //Toast.makeText(, "Oops something went wrong!", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void followLink(Context context) {

        if (!this.getDomain().startsWith("self") && !this.getDomain().startsWith("i.redd")) {

            Intent followLinkIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(this.getLink()));
            context.startActivity(followLinkIntent);

        } else {
            Toast.makeText(context, "No link to follow!", Toast.LENGTH_SHORT).show();
        }
    }

    public void share(Context context) {

        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, this.getLink());
        shareIntent.setType("text/plain");

        Intent sendIntent = Intent.createChooser(shareIntent, null);
        context.startActivity(sendIntent);
    }

    public void comment(Context context) {

        Intent commentsActivityIntent = new Intent(context ,CommentsActivity.class);
        commentsActivityIntent.putExtra("post", this);

        context.startActivity(commentsActivityIntent);

    }

}

