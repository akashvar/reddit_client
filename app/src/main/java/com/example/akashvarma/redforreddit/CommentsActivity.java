package com.example.akashvarma.redforreddit;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.unnamed.b.atv.model.TreeNode;
import com.unnamed.b.atv.view.AndroidTreeView;

import org.json.JSONArray;

import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CommentsActivity extends AppCompatActivity {

    private Post post;
    private static Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);

        context = this;

        post = (Post) getIntent().getSerializableExtra("post");

        fetchComments();

    }

    private void fetchComments() {

        Retrofit.Builder b = new Retrofit.Builder()
                .baseUrl("https://oauth.reddit.com/r/")
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = b.build();

        final RedditClient client = retrofit.create(RedditClient.class);

        String token = AuthHeaders.ACCESS_TOKEN;

        String id = post.getId().substring(3);

        final Call<JsonArray> commentsCall = client.getComments(token, post.getSubreddit(), id);

        commentsCall.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {

                if (response.code() == 200) {

                    TreeNode root = TreeNode.root();

                    JsonArray array = response.body().get(1).getAsJsonObject()
                            .get("data").getAsJsonObject()
                            .get("children").getAsJsonArray();

                    Parser.parseComments(array, root);

                    AndroidTreeView tree = new AndroidTreeView(CommentsActivity.context, root);

                    ConstraintLayout containerLayout = findViewById(R.id.comments_layout);
                    containerLayout.addView(tree.getView());
                }

            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {

                Log.d("FETCH_COMMENTS", "failed to fetch comments");
            }
        });

    }

    private static class Parser {

        private static void parseComments(JsonArray c, TreeNode parent) {

            for(int i=0;i<c.size();i++){

                if (c.get(i).getAsJsonObject().get("kind") == null) continue;
                if (!c.get(i).getAsJsonObject().get("kind").getAsString().equalsIgnoreCase("t1")) {
                    //TODO fetch more comments from tree
                    continue;
                }
                JsonObject data = c.get(i).getAsJsonObject().get("data").getAsJsonObject();
                //JSONObject data=c.getJSONObject(i).getJSONObject("data");
                Comment comment=loadComment(data);

                if(comment.getAuthor() != null) {
                    TreeNode node = new TreeNode(comment).setViewHolder(new CommentHolder(CommentsActivity.context));
                    //comments.add(comment);
                    parent.addChild(node);
                    addReplies(data,node);
                }
            }
        }

        private static void addReplies(JsonObject data, TreeNode parent) {

            try {

                if (!data.get("replies").isJsonObject() || data.get("replies").getAsJsonObject().size() < 2) {
                    // This means the comment has no replies
                    return;
                }

                JsonArray array = data.get("replies").getAsJsonObject()
                        .get("data").getAsJsonObject()
                        .get("children").getAsJsonArray();

                parseComments(array, parent);

                //process(comments, r, level);
            } catch(Exception e){
                Log.d("ERROR","addReplies : " + e);
            }

        }

        private static Comment loadComment(JsonObject data) {

            Comment comment = new Comment();

            try {

                comment.setId(data.get("name").getAsString());
                comment.setContent(data.get("body").getAsString());
                comment.setAuthor(data.get("author").getAsString());
                comment.setVotes(String.valueOf(data.get("ups").getAsInt() - data.get("downs").getAsInt()));
                comment.setCommentTime(new Date((long)data
                        .get("created_utc").getAsDouble())
                        .toString());
                //comment.level=level;
            } catch(Exception e) {

                Log.d("ERROR","Unable to parse comment : "+e);
            }

            return comment;
        }
    }
}
