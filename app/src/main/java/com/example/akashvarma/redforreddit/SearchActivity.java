package com.example.akashvarma.redforreddit;

import android.app.SearchManager;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SearchActivity extends AppCompatActivity {

    private PostAdapter mAdapter;
    private List<Post> mPosts;
    private RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        String query = checkQuery(getIntent());

        mPosts = new ArrayList<Post>();

        mRecyclerView = findViewById(R.id.results_list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        if (query != null) performSearch(query);
        else {
            Toast.makeText(this, "Enter a search query!", Toast.LENGTH_SHORT).show();
            finish();
        }

    }

    private String checkQuery(Intent intent) {

        String query;

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            query = intent.getStringExtra(SearchManager.QUERY);
        } else {
            query = null;
        }

        return query;
    }

    private void performSearch(String query) {

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("https://oauth.reddit.com/")
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();

        final RedditClient client = retrofit.create(RedditClient.class);

        //String refreshToken = oauthPrefs.getString("RefreshToken", null);
        //String basicAuthCred = oauthPrefs.getString("AuthHeader", null);

        Call<JsonObject> searchCall = client.fetchSearchResults(
                AuthHeaders.ACCESS_TOKEN,
                query
        );

        searchCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                if (response.code() == 200) {
                    mPosts = getPostList(response.body());
                    mAdapter = new PostAdapter(mPosts, SearchActivity.this);
                    mRecyclerView.setAdapter(mAdapter);
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(SearchActivity.this, "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private List<Post> getPostList(JsonObject jsonObject) {

        List<Post> posts = new ArrayList<>();

        JsonArray array = jsonObject.getAsJsonObject("data").getAsJsonArray("children");

        for (int i = 0; i < array.size(); i++) {
            JsonObject obj = array.get(i).getAsJsonObject().getAsJsonObject("data");

            Post post = new Post(
                    obj.get("name").getAsString(),
                    obj.get("url").getAsString(),
                    obj.get("title").getAsString(),
                    obj.get("selftext").getAsString(),
                    obj.get("subreddit").getAsString(),
                    "u/" + obj.get("author").getAsString(),
                    obj.get("domain").getAsString()
            );

            if (obj.get("preview") != null) post.setThumbnail(parsePostImage(obj));

            if (!obj.get("likes").isJsonNull()) {
                post.setVoted(obj.get("likes").getAsBoolean());
            } else {
                post.setVoted(null);
            }

            posts.add(post);
        }

        return posts;
    }

    private String parsePostImage(JsonObject jsonObject) {

        JsonObject preview = jsonObject.getAsJsonObject("preview");
        JsonArray images = preview.getAsJsonArray("images");
        JsonObject element = images.get(0).getAsJsonObject();
        JsonArray resolutions = element.getAsJsonArray("resolutions");
        JsonObject highRes = resolutions.get(resolutions.size() - 1).getAsJsonObject();
        String url = highRes.get("url").getAsString();

        url = url.replace("&amp;", "&");

        return url;

    }
}
