package com.example.akashvarma.redforreddit;

import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HomeScreenActivity extends AppCompatActivity {

    private SharedPreferences oauthPrefs;
    private String refreshToken;
    private String accessToken;
    private String httpAuthHeader;

    private fetchTask fetch;

    private RecyclerView mRecyclerView;
    private PostAdapter mAdapter;

    private List<Post> mPosts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);

        oauthPrefs = getSharedPreferences("MYOAUTHPREFERENCES", MODE_PRIVATE);

        mPosts = new ArrayList<Post>();

        mRecyclerView = findViewById(R.id.post_list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        fetch = new fetchTask();

        refreshToken = oauthPrefs.getString("RefreshToken", null);
        httpAuthHeader = oauthPrefs.getString("AuthHeader", null);
        //accessToken = oauthPrefs.getString("AccessToken", null);

        this.handleOAuth();

        fetch.fetchFrontPage();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        //android.widget.SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();

        searchView.setSearchableInfo(searchManager.getSearchableInfo(new
                ComponentName(this,SearchActivity.class)));

        return true;
    }

    //handles first time login and subsequent authentications
    private void handleOAuth() {

        Long l = (System.currentTimeMillis() / 1000);

        //First time login
        if (!oauthPrefs.contains("AccessToken")) {

            Intent loginIntent = new Intent(this, LoginActivity.class);
            startActivity(loginIntent);

            //previous token expired, get refreshed token
        } else if (oauthPrefs.getLong("Expiration", Long.MIN_VALUE) < l) {

            Retrofit.Builder builder = new Retrofit.Builder()
                    .baseUrl("https://www.reddit.com/")
                    .addConverterFactory(GsonConverterFactory.create());

            Retrofit retrofit = builder.build();

            final RedditClient client = retrofit.create(RedditClient.class);

            //String refreshToken = oauthPrefs.getString("RefreshToken", null);
            //String basicAuthCred = oauthPrefs.getString("AuthHeader", null);

            Call<AccessToken> refreshTokenCall = client.refreshToken(
                    oauthPrefs.getString("AuthHeader", null),
                    "refresh_token",
                    oauthPrefs.getString("RefreshToken", null)
            );

            refreshTokenCall.enqueue(new Callback<AccessToken>() {
                @Override
                public void onResponse(Call<AccessToken> call, Response<AccessToken> response) {

                    if (response.code() == 200) {

                        SharedPreferences.Editor edit = oauthPrefs.edit();

                        edit.putString("AccessToken", "Bearer " + response.body().getAccessToken());
                        AuthHeaders.ACCESS_TOKEN = "Bearer " + response.body().getAccessToken();
                        AuthHeaders.REFRESH_TOKEN = response.body().getRefreshToken();
                        edit.putLong("Expiration", Long.parseLong(response.body().getExpiryTime()) + (System.currentTimeMillis() / 1000));
                        edit.commit();
                        //Toast.makeText(HomeScreenActivity.this, "refresh token updated", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<AccessToken> call, Throwable t) {
                    Toast.makeText(HomeScreenActivity.this, "Couldn't authenticate!", Toast.LENGTH_LONG).show();
                }
            });

        } else {
            AuthHeaders.ACCESS_TOKEN = oauthPrefs.getString("AccessToken", null);
            AuthHeaders.REFRESH_TOKEN = oauthPrefs.getString("RefreshToken", null);
        }


    }

    private class fetchTask {

        private String BASE_URL = "https://oauth.reddit.com/";
        //private List<Post> mPosts;

        private void fetchFrontPage() {

            Retrofit.Builder b = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create());

            Retrofit r = b.build();

            final RedditClient c = r.create(RedditClient.class);

            Call<JsonObject> frontPageCall = c.getFrontPage(oauthPrefs.getString("AccessToken", null), "all");

            frontPageCall.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    if (response.code() == 200) {
                        mPosts = getPostList(response.body());
                        mAdapter = new PostAdapter(mPosts, HomeScreenActivity.this);
                        mRecyclerView.setAdapter(mAdapter);
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Toast.makeText(HomeScreenActivity.this, "oops something went wrong", Toast.LENGTH_LONG).show();
                }
            });
        }

        private List<Post> getPostList(JsonObject jsonObject) {

            List<Post> posts = new ArrayList<>();

            JsonArray array = jsonObject.getAsJsonObject("data").getAsJsonArray("children");

            for (int i = 0; i < array.size(); i++) {
                JsonObject obj = array.get(i).getAsJsonObject().getAsJsonObject("data");

                Post post = new Post(
                        obj.get("name").getAsString(),
                        obj.get("url").getAsString(),
                        obj.get("title").getAsString(),
                        obj.get("selftext").getAsString(),
                        obj.get("subreddit").getAsString(),
                        "u/" + obj.get("author").getAsString(),
                        obj.get("domain").getAsString()
                );

                if (obj.get("preview") != null) post.setThumbnail(parsePostImage(obj));

                if (!obj.get("likes").isJsonNull()) {
                    post.setVoted(obj.get("likes").getAsBoolean());
                } else {
                    post.setVoted(null);
                }

                posts.add(post);
            }

            return posts;
        }

        private String parsePostImage(JsonObject jsonObject) {

            JsonObject preview = jsonObject.getAsJsonObject("preview");
            JsonArray images = preview.getAsJsonArray("images");
            JsonObject element = images.get(0).getAsJsonObject();
            JsonArray resolutions = element.getAsJsonArray("resolutions");
            JsonObject highRes = resolutions.get(resolutions.size() - 1).getAsJsonObject();
            String url = highRes.get("url").getAsString();

            url = url.replace("&amp;", "&");

            return url;

        }


    }
}
