package com.example.akashvarma.redforreddit;

import com.google.gson.annotations.SerializedName;

class AccessToken {

    @SerializedName("access_token")
    private String accessToken;

    @SerializedName("token_type")
    private String tokenType;

    @SerializedName("scope")
    private String scope;

    @SerializedName("expires_in")
    private String expiryTime;

    @SerializedName("refresh_token")
    private String refreshToken;

    public String getRefreshToken() {
        return refreshToken;
    }

    public String getScope() {
        return scope;
    }

    public String getExpiryTime() {
        return expiryTime;
    }

    public String getAccessToken() {
        return accessToken;
    }


    public String getTokenType() {
        return tokenType;
    }
}

