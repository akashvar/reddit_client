package com.example.akashvarma.redforreddit;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;

import java.util.List;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class PostAdapter
        extends RecyclerView.Adapter<PostAdapter.PostHolder> {

    private List<Post> mPosts;
    private Context mContext;

    public static class PostHolder extends RecyclerView.ViewHolder {
        private View rowView;

        public PostHolder(View v) {
            super(v);
            rowView = v;
        }
    }

    public PostAdapter(List<Post> posts, Context context) {
        mPosts = posts;
        mContext = context;
    }

    @Override
    public PostHolder onCreateViewHolder(ViewGroup parent, int type) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.post_list_item, parent, false);
        PostHolder holder = new PostHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(PostHolder holder, final int position) {

        holder.rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(mContext, "Row at position " + String.valueOf(position) + " clicked.", Toast.LENGTH_SHORT).show();
            }
        });

        final Post post = mPosts.get(position);

        ((TextView)holder.rowView.findViewById(R.id.post_title)).setText(post.getTitle());
        ((TextView)holder.rowView.findViewById(R.id.post_text)).setText(post.getContent());
        ((TextView)holder.rowView.findViewById(R.id.post_subreddit)).setText(post.getSubreddit());
        ((TextView)holder.rowView.findViewById(R.id.post_author)).setText(post.getAuthor());

        ImageView imageView = (ImageView)holder.rowView.findViewById(R.id.post_image);

        if (post.getThumbnail() == null) {
            imageView.setVisibility(View.GONE);

        } else if (post.getThumbnail().equals("self") || post.getThumbnail().equals("default")) {
            imageView.setVisibility(View.GONE);

        } else  {
            String image = post.getThumbnail();
            Picasso.get().load(image).into(imageView);
        }

        initActionButtons(holder, post);

    }

    private void initActionButtons(PostHolder holder, final Post post) {

        final Button up = holder.rowView.findViewById(R.id.post_up);
        final Button down = holder.rowView.findViewById(R.id.post_down);
        final Button comment = holder.rowView.findViewById(R.id.post_comment);
        final Button share = holder.rowView.findViewById(R.id.post_share);
        final Button link = holder.rowView.findViewById(R.id.post_link);

        if (post.getVoted() != null) {
            if (post.getVoted()) up.setBackgroundColor(Color.GREEN);
            else if (!post.getVoted()) down.setBackgroundColor(Color.RED);
        }


        up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                post.upvote();
            }
        });

        down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                post.downvote();
            }
        });

        comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                post.comment(mContext);
            }
        });

        link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                post.followLink(mContext);
            }
        });

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                post.share(mContext);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mPosts.size();
    }

}

