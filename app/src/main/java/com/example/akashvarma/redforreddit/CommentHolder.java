package com.example.akashvarma.redforreddit;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.unnamed.b.atv.model.TreeNode;

public class CommentHolder extends TreeNode.BaseNodeViewHolder<Comment> {

    public CommentHolder(Context context) {
        super(context);
    }

    @Override
    public View createNodeView(TreeNode node, Comment value) {

        final LayoutInflater inflater = LayoutInflater.from(context);

        final View view = inflater.inflate(R.layout.comment_list_item, null, false);

        TextView commentBody = view.findViewById(R.id.comment_body);
        commentBody.setText(value.getContent());

        TextView commentAuthor = view.findViewById(R.id.comment_author);
        commentAuthor.setText(value.getAuthor());

        TextView commentTime = view.findViewById(R.id.comment_time);
        commentTime.setText(value.getCommentTime());

        TextView commentVotes = view.findViewById(R.id.comment_votes);
        commentVotes.setText(value.getVotes());

        initActionButtons(view, value);

        return view;
    }

    private void initActionButtons(View view, final Comment comment) {

        Button upComment = view.findViewById(R.id.comment_up);
        Button downComment = view.findViewById(R.id.comment_down);
        Button replyComment = view.findViewById(R.id.comment_reply);

        upComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comment.upvote();
            }
        });

        downComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comment.downvote();
            }
        });

        replyComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }
}
